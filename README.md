1. create virtual environment.
       $ virtualenv oscar
       $ . ./oscar/bin/activate
2. Install oscar and than create project.
       $ mkvirtualenv oscar
       $ pip install django-oscar
       $ django-admin.py startproject frobshop

3. import all default oscar settings.
4. make changes in template and installed_apps according to your need. 
5. alter urls.py to oscar's url and can also add debugging.
6. setup backend.(haystack can be used for this)
7. define database and ports on which it's running, it better to use version 9.4/9.5 of postgres.
8. Start your application using 
       $ python manage.py migrate
       $ python manage.py runserver
9. You will be able to see application on 8000 port.
10.Create superuser to login admin panel.
11.Using admin panel you can now define products,offer,coupons and than we can create orders.
